export  interface  ProductModel  {
	id:number,
	name:string,
	description:string,
	create_by:string,
	create_dt:Date,
	update_by:string,
	update_dt:Date

}