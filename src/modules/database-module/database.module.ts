import { Module } from '@nestjs/common';
import { TypeOrmModule} from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { InventarioModule } from  '../inventario/inventario.module';
import { ProductEntity } from '../../entity/product.entity';

@Module({
	imports:[
		TypeOrmModule.forRoot({
			type: 'mysql',// type database  
			host:'localhost',//server  database
			port:3306, // port the database
			username:'root', //user database
			password: '123456', 
			database:'inventario',
			entities:[
				ProductEntity
			 ],
			 synchronize:true
		}),
		InventarioModule,
		
	]
	
})
export class DatabaseModule {
	constructor( private readonly connection:Connection){
	}
}
