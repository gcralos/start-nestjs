import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProductServices } from './product-services/product.service';
import { ProductEntity } from 'src/entity/product.entity';
import { ProductController } from './product-controller/product.controller';

@Module({
	imports:[
		TypeOrmModule.forFeature([ProductEntity])
	],
	controllers: [
		ProductController
	],
	providers: [ProductServices]
})
export class InventarioModule {}
