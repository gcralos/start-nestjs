import  { Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity('product')
export class ProductEntity {

	@PrimaryGeneratedColumn()
	id: number;

	@Column()
	name: string;

	@Column()
	description:string;

	@Column()
	create_by:string;

	@Column()
	create_dt:Date;

	@Column()
	update_by:string;

	@Column()
	update_dt:Date;

}
